<?php
/**
 * InstallData
 *
 * @copyright Copyright © 2017 Firetoss. All rights reserved.
 * @author    sgmadsen@gmail.com
 */

namespace Firetoss\Navigation\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Menu setup factory
     *
     * @var MenuSetupFactory
     */
    protected $menuSetupFactory;

    /**
     * Init
     *
     * @param MenuSetupFactory $menuSetupFactory
     */
    public function __construct(MenuSetupFactory $menuSetupFactory)
    {
        $this->menuSetupFactory = $menuSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) //@codingStandardsIgnoreLine
    {
        /** @var MenuSetup $menuSetup */
        $menuSetup = $this->menuSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $menuSetup->installEntities();
        $entities = $menuSetup->getDefaultEntities();
        foreach ($entities as $entityName => $entity) {
            $menuSetup->addEntityType($entityName, $entity);
        }

        $setup->endSetup();
    }
}
